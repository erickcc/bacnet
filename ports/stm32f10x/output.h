#ifndef OUTPUT_H
#define OUTPUT_H

void output_init(void);
void set_output_pwm(uint8_t id, uint8_t value);

#endif
