#ifndef INPUT_H
#define INPUT_H

void input_init(void);
uint8_t get_input_value(uint8_t index);
uint16_t get_analog_value(uint8_t index);

#endif
